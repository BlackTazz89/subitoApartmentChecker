from setuptools import setup, find_packages

setup(
    name="SubitoApartmentChecker",
    version="1.0.0",
    description="A simple script to check if there are some interesting apartment on Subito.it",

    author="Francesco Marino",
    author_email="blacktazz89@gmail.com",

    license="GPL",

    keywords="Subito.it apartment",

    packages=find_packages(),

    install_requires=["beautifulsoup4>=4.4.1", "python-dateutil>=2.4.2", "google-api-python-client>=1.4.2", "DateTime>=4.0.1"],

    entry_points={
        "console_scripts": [
        ],
    },
)
