import os, sys, logging
from subitoApartmentChecker.SubitoApartmentChecker import SubitoApartmentChecker
from google.GmailMessageSender import GmailMessageSender
from google.GoogleOauthTokenRetriever import GoogleOauthTokenRetriever

# Configuring root logger
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
                    datefmt="%m-%d %H:%M"   ,
                    filename="/var/log/subitoApartmentChecker/subitoApartmentChecker.log",
                    filemode='a')

logger = logging.getLogger(__name__)

messageTemplate = u"""
Title  --> {0}
Link   --> {1}
Price  --> {2}
Specs  --> {3}
Time   --> {4}
"""

current_path = os.path.abspath(os.path.dirname(sys.argv[0]))

credential_dir = os.path.join(current_path, ".credentials")
logger.debug("Searching for credentials")
if not os.path.exists(credential_dir):
    logger.debug("Credentials folder not present. Creating it")
    os.makedirs(credential_dir)
credential_path = os.path.join(credential_dir, "subito_apartment_checker_access_token.json")

client_secret_path = os.path.join(current_path, "client_secret.json")

logger.info("Try to retrieve credentials")

credentials = GoogleOauthTokenRetriever(client_secret_path, credential_path, "http://blacktazz.ddns.net:8080",
                                        "https://www.googleapis.com/auth/gmail.send",
                                        "offline").get_credentials()

logger.info("credentias retrieved")

gmail_sender = GmailMessageSender(credentials)
logger.debug("gmail sender created successfully")

subitoChecker = SubitoApartmentChecker()
logger.debug("subito checker created successfully")

message_to_send = u""
for adData in subitoChecker.get_ads_data_by_preferences(max_price=500, min_square_meters=50, max_elapsed_minutes=60):
    formatted_data = messageTemplate.format(*adData)
    logger.debug("Data to send: %s", formatted_data)
    message_to_send += (formatted_data + u"\n")

if message_to_send:
    gmail_sender.send_message("blacktazz89@gmail.com", "blacktazz89@gmail.com", "New Apartment - (ByTazSubitoBot)",
                              message_to_send.encode("utf-8"))
    logger.info("Message sent")
