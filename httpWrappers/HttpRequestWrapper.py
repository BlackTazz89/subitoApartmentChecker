import urllib
import urllib2
import gzip
import logging
from StringIO import StringIO

logger = logging.getLogger(__name__)


def send_request(base_url, headers={}, query_param={}, post_data={}):
    logger.debug("Forging a request object")
    req = urllib2.Request((base_url + "?" + urllib.urlencode(query_param)) if len(query_param) else base_url,
                          headers=headers)
    logger.debug("Send request")
    return urllib2.urlopen(req, urllib.urlencode(post_data)) if len(post_data) else urllib2.urlopen(req)


def decode_if_gzipped(response):
    if response.info().get("Content-Encoding") == "gzip":
        logger.debug("Content is gzipped, decompress it")
        return gzip.GzipFile(fileobj=StringIO(response.read()))
    else:
        logger.debug("Content isn't gzipped")
        return response
