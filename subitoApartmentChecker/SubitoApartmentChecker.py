import re, logging
from bs4 import BeautifulSoup
import dateutil.parser
from httpWrappers.HttpRequestWrapper import send_request, decode_if_gzipped

logger = logging.getLogger(__name__)


class SubitoApartmentChecker(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def _get_subito_ts_rent_page():
        logger.debug("Getting subito rent page")
        headers = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                   'accept-encoding': 'gzip, deflate, sdch',
                   'accept-language': 'it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4',
                   'cache-control': 'no-cache',
                   'connection': 'keep-alive',
                   'host': 'www.subito.it',
                   'pragma': 'no-cache',
                   'upgrade-insecure-requests': '1',
                   'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/45.0.2454.85 Chrome/45.0.2454.85 Safari/537.36'
                   }

        response = send_request(
            'http://www.subito.it/annunci-friuli-venezia-giulia/affitto/appartamenti/friuli-venezia-giulia/trieste/',
            headers)
        logger.debug("Response code: %s", response.getcode())

        return BeautifulSoup(decode_if_gzipped(response), 'html.parser', from_encoding="utf-8")

    @staticmethod
    def _get_ads(subito_rent_page):
        logger.debug("Getting ads")
        return subito_rent_page.find_all('div', class_='item_list_section item_description')

    @staticmethod
    def _extract_data_by_preferences(ad, max_price=None, min_square_meters=None, max_elapsed_minutes=None):
        logger.debug("Getting data from ads")
        ad_anchor = ad.find('a')
        title = ad_anchor['title']
        link = ad_anchor['href']
        maybe_price = ad.select('span.item_price')
        maybe_specs = ad.select('span.item_specs')
        maybe_date = ad.select('time')

        price = maybe_price[0].string if maybe_price else None
        parsed_price = int(re.sub(r'([\.\D])', r'', price)) if price else None
        specs = re.sub(r'\D*(\d.*)', r'\1', maybe_specs[0].string) if maybe_specs else None
        parsed_square_meters = int(re.match(r'(\d*)\s*mq', specs).group(1)) if specs else None
        date = maybe_date[0]['datetime'] if maybe_date else None
        parsed_date = dateutil.parser.parse(date) if date else None

        elapsed_minutes = (parsed_date.now().replace(second=0, microsecond=0) -
                           parsed_date.replace(second=0, microsecond=0)).total_seconds() / 60

        logger.debug("Found -> Price = %s, Square meters = %s, Date = %s", parsed_price, parsed_square_meters, date)

        if (not max_price or parsed_price <= max_price) and \
                (not min_square_meters or parsed_square_meters >= min_square_meters) and \
                (not max_elapsed_minutes or elapsed_minutes <= max_elapsed_minutes):
            logger.debug("Returning datas")
            return [title, link, price, specs, date]
        logger.debug("Ignoring datas")
        return None

    def get_ads_data_by_preferences(self, max_price=None, min_square_meters=None, max_elapsed_minutes=None):
        all_ads = self._get_ads(self._get_subito_ts_rent_page())
        self.logger.debug("Ads found: %s", len(all_ads))
        return filter(lambda x: x is not None,
                      [self._extract_data_by_preferences(ad, max_price, min_square_meters, max_elapsed_minutes) for ad
                       in
                       all_ads])
