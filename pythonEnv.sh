#!/bin/bash

createVirtualEnv() {

wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-13.1.2.tar.gz --output-document=virtualEnv.tar.gz

mkdir virtualEnv

tar --extract --gzip --verbose --file virtualEnv.tar.gz --directory virtualEnv --strip-components 1

python virtualEnv/virtualenv.py .

rm -rf virtualEnv.tar.gz virtualEnv

}

cleanAll() {

rm -rf bin include lib local share SubitoApartmentChecker.egg-info pip-selfcheck.json

}

setup() {

bin/pip install --editable .

}

unset optionPassed

while getopts ":vcs" opt; do
  case $opt in
    v)
      optionPassed=true
      echo "Stating the creation of virtualEnv" >&2
      createVirtualEnv
      ;;
    c)
      optionPassed=true
      echo "Removing virtualEnv files" >&2
      cleanAll
      ;;
    s)
      optionPassed=true
      echo "Installing needed python packages" >&2
      setup
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -z "$optionPassed" ]
then
   echo "Usage:
        pythonEnv -v to create a base virtualEnv
        pythonEnv -c to clean all virtualEnv files
        pythonEnv -s to execute setup.py and install all needed packages" >&2
   exit
fi
