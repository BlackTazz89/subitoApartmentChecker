import base64
import httplib2
import logging
from email.mime.text import MIMEText
from googleapiclient import errors, discovery

logger = logging.getLogger(__name__)


class GmailMessageSender(object):
    def __init__(self, credentials):
        self.credentials = credentials
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def _create_message(sender, to, subject, message_text):
        message = MIMEText(message_text)
        message["from"] = sender
        message["to"] = to
        message["subject"] = subject
        logger.debug("Sending message from %s, to %s", sender, to)
        return {"raw": base64.urlsafe_b64encode(message.as_string())}

    def send_message(self, sender, to, subject, message_text):
        if not hasattr(self, "service"):
            self.logger.debug("Creating gmail service object")
            http = self.credentials.authorize(httplib2.Http())
            self.logger.debug("Credentials headers appended to http instance")
            self.service = discovery.build("gmail", "v1", http=http)
            self.logger.debug("Gmail service created")
        try:
            self.service.users().messages() \
                .send(userId="me", body=self._create_message(sender, to, subject, message_text)) \
                .execute()
            self.logger.debug("Email sent")
        except errors.HttpError, error:
            self.logger.error("Couldn't sent the email. Error: %s", error)
