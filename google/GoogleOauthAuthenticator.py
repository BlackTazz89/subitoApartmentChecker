import argparse
from oauth2client.file import Storage
from oauth2client import client
from oauth2client import tools


class GoogleOauthAuthenticator(object):
    def __init__(self, client_secret_path, credential_path, application_name, scopes):
        self.client_secret_path = client_secret_path
        self.credential_path = credential_path
        self.application_name = application_name
        self.scopes = scopes

    def generate_access_token(self, force_reauth=False):
        parser = argparse.ArgumentParser(parents=[tools.argparser])
        flags = parser.parse_args()

        store = Storage(self.credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid or force_reauth:
            flow = client.flow_from_clientsecrets(self.client_secret_path, self.scopes)
            flow.user_agent = self.application_name
            credentials = tools.run_flow(flow, store, flags)
        return credentials
