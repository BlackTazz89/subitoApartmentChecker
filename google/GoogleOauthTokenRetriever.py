import json, httplib2, datetime, logging
from BaseHTTPServer import HTTPServer
from webbrowser import open_new
from oauth2client.file import Storage
from oauth2client.client import OAuth2Credentials
from google.GoogleOauthTokenHandler import GoogleOauthTokenHandler


class GoogleOauthTokenRetriever(object):
    def __init__(self, client_secret_path, credential_path, redirect_uri, scope, access_type):
        self.client_secret_path = client_secret_path
        self.credential_path = credential_path
        self.redirect_uri = redirect_uri
        self.scope = scope
        self.access_type = access_type
        self.logger = logging.getLogger(__name__)

    def get_credentials(self):
        self.logger.debug("Getting credentials from storage")
        store = Storage(self.credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            self.logger.debug("Credentials invalid or absent")
            response = self._make_oauth_flow()
            token_expiry = (datetime.datetime.utcnow() + datetime.timedelta(seconds=int(response['expires_in'])))
            credentials = OAuth2Credentials(response["access_token"], self.client_id, self.client_secret,
                                            response["refresh_token"],
                                            token_expiry, "https://accounts.google.com/o/oauth2/token",
                                            "subito_apartment_checker",
                                            revoke_uri="https://accounts.google.com/o/oauth/revoke",
                                            token_info_uri="https://www.googleapis.com/oauth2/v2/tokeninfo",
                                            scopes=[scope for scope in self.scope.split(" ")])
            self.logger.debug("Credentials retrieved")
        elif credentials.access_token_expired:
            self.logger.debug("Credentials expired. Refreshing it")
            credentials.refresh(httplib2.Http())
            self.logger.debug("Credentials refreshed")
        self.update_credentials_storage(credentials)
        return credentials

    def _make_oauth_flow(self):
        with open(self.client_secret_path) as client_data:
            data = json.load(client_data)
            self.client_id = data["web"]["client_id"]
            self.client_secret = data["web"]["client_secret"]

        self.logger.debug("Forging authorization uri to redirect user to")
        authorization_uri = ("https://accounts.google.com/o/oauth2/auth?"
                             + "client_id=" + self.client_id
                             + "&redirect_uri=" + self.redirect_uri
                             + "&scope=" + self.scope
                             + "&response_type=code"
                             + "&access_type=" + self.access_type)

        self.logger.debug("Open authorization uri in default browser")
        open_new(authorization_uri)

        self.logger.debug("Waiting for user response")
        httpServer = HTTPServer(("0.0.0.0", 8080),
                                lambda request, address, server: GoogleOauthTokenHandler(request, address, server,
                                                                                         self.client_id,
                                                                                         self.client_secret,
                                                                                         self.redirect_uri))
        httpServer.handle_request()

        self.logger.debug("Request handled")

        return json.load(httpServer.client_credentials_response)

    def update_credentials_storage(self, credentials):
        store = Storage(self.credential_path)
        store.put(credentials)
        self.logger.debug("Credentials updated")
