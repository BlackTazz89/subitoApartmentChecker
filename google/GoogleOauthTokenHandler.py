import logging
from BaseHTTPServer import BaseHTTPRequestHandler
from httpWrappers.HttpRequestWrapper import send_request


class GoogleOauthTokenHandler(BaseHTTPRequestHandler):
    def __init__(self, request, address, server, client_id, client_secret, redirect_uri):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri
        self.logger = logging.getLogger(__name__)
        BaseHTTPRequestHandler.__init__(self, request, address, server)

    def do_GET(self):
        self.logger.debug("Handling user response")
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        if 'code' in self.path:
            self.logger.debug("Authorization token found")
            self.auth_code = self.path.split('=')[1]
            self.wfile.write("<html><h3>You may now close this window. </h3></html>")
            access_uri = "https://accounts.google.com/o/oauth2/token"
            headers = {
                "host": "accounts.google.com",
                "content-type": "application/x-www-form-urlencoded"
            }
            post_data = {
                "grant_type": "authorization_code",
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "redirect_uri": self.redirect_uri,
                "code": self.auth_code
            }

            self.logger.debug("Posting data to get access_token")

            self.server.client_credentials_response = send_request(access_uri, headers=headers,
                                                                   post_data=post_data)

            self.logger.debug("Access token received")
